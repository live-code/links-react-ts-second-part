import React from 'react';
import { NavLink } from 'react-router-dom';

export const Navbar = () => {
  return (
    <div>
      <NavLink to="/">
        <button>Render Flow</button>
      </NavLink>
      <NavLink to="/login">
        <button>login</button>
      </NavLink>

      <NavLink to="/tvmaze">
        <button>TV Maze</button>
      </NavLink>
      <NavLink to="/context">
        <button>Context</button>
      </NavLink>
      <NavLink to="/useReducer">
        <button>useReducer</button>
      </NavLink>

    </div>
  )
}
