import React, { lazy, Suspense } from 'react';
import { PageDemoRender } from './features/PageDemoRender';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { PageContext } from './features/PageContext';
import { PageDemoTVMaze } from './features/tv-maze/PageDemoTVMaze';
import { Navbar } from './core/Navbar.component';
import css from './App.module.css';
import { PageUseReducer } from './features/use-reducer/PageUseReducer';

const PageLogin = lazy(() => import('./features/PageLogin'));

function App() {
  return (
      <BrowserRouter>
        <Navbar />

        <div className={css.container}>
          <Suspense fallback={<div>Loading...</div>}>
            <Switch>
              <Route path="/login">
                <PageLogin />
              </Route>

              <Route path="/context" component={PageContext} />

              <Route path="/tvmaze">
                <PageDemoTVMaze />
              </Route>

              <Route path="/useReducer">
                <PageUseReducer />
              </Route>

              <Route path="*">
                <PageDemoRender />
              </Route>
            </Switch>
          </Suspense>
        </div>
      </BrowserRouter>
  );
}

export default App;
