import React, { memo, useState } from 'react';

export const PageDemoRender: React.FC = () => {
  const [counter, setCounter] = useState(0)

  const inc = () => {
    setCounter(counter + 1);
  }

  function dec() {
    setCounter(counter - 1);
  }

  console.log('PARENT: render Home')
  return <div>
    {counter}
    <hr/>
    <button onClick={inc}>+</button>
    <button onClick={dec}>-</button>
    <hr/>
    <Panel title={counter} />
    <List />
  </div>
}


// List.tsx
const ListCompo: React.FC = () => {
  console.log('CHILD: render list')
  return <div>
    List
    <Title>{1+1}</Title>
  </div>
}

export const List = memo(ListCompo);

export const Title: React.FC = (props) => {
  console.log('CHILD -> PANEL: render title')
  return <h1>{props.children}</h1>
}


// ===========
// Panel.tsx
interface PanelProps {
  title: number | string;
}
export const Panel: React.FC<PanelProps> = memo((props) => {
  console.log('CHILD: render panel')
  return <div>
    Panel: {props.title}
  </div>
})


