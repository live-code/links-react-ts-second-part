import React, { createContext, useContext, useState } from 'react';

const themes = {
  light: {
    color: '#000000',
    background: '#eeeeee',
  },
  dark: {
    color: '#cccccc',
    background: '#000000',
  },
};
/*
type ThemeProps = {
  foreground: string,
  background: string,
}
*/

type ThemeProps = typeof themes.dark;

export const ThemeContext = createContext<ThemeProps>(themes.light);

// 1st
export const PageContext: React.FC = () => {
  const [themeState, setThemeState] = useState<ThemeProps>(themes.dark)

  return (<div>
    <ThemeContext.Provider value={themeState}>
      <button onClick={() => setThemeState(themes.dark)}>Dark</button>
      <button onClick={() => setThemeState(themes.light)}>Light</button>

      <hr/>
      My Toolbar
      <ToolBar />
    </ThemeContext.Provider>



  </div>);
}


// 2nd Toolbar.tsx
const ToolBar: React.FC = () => {
  return <span>
    <ThemeButton>one</ThemeButton>
    <ThemeButton>two</ThemeButton>
  </span>
}

// 3rd ThemeButton
const ThemeButton: React.FC = props => {
  const theme = useContext(ThemeContext)
  return <button style={theme}>{props.children}</button>
}
