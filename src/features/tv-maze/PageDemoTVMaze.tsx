import React from 'react';
import { TVMazeResults } from './components/tvmaze-results/TVMazeResults';
import { TVMazeForm } from './components/tvmaze-form/TVMazeForm';
import { useTVMaze } from './hooks/useTVMaze';

export const PageDemoTVMaze: React.FC = () => {

  const { result, search, openLink } = useTVMaze();

  return (
    <div>
      <TVMazeForm search={search} />
      <TVMazeResults result={result} open={openLink} />
    </div>
  )
};
