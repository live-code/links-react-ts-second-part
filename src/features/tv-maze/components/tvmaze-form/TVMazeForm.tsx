import React, { useState } from 'react';

export interface TVMazeFormProps {
  search: (text: string) => void;
}
export const TVMazeForm: React.FC<TVMazeFormProps> = (props) => {
  const [ text, setText ] = useState<string>('');

  const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.currentTarget.value)
  };

  const submitHandler = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    props.search(text);
  };

  return (
    <form onSubmit={submitHandler}>
      <input
        type="text"
        value={text}
        onChange={onChangeHandler}
        placeholder="Search TV series"
        name="blabla"
      />
      <button type="submit">SEARCH</button>
    </form>
  )
}
