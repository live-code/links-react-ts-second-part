import React from 'react';
import { Image } from '../../model/series';
import css from './TvMazeThumb.module.css';

export interface TVMazeThumbProps {
  name: string;
  image: Image;
  thumbClick: () => void;
}

export const TVMazeThumb: React.FC<TVMazeThumbProps> = (props) => {
  const { image, name, thumbClick } = props;
  return (
    <div className={css.movie} onClick={thumbClick}>
      <div>
        {
          image ?
            <img src={image?.medium} alt={name} width="100%"/> :
            <NoImage />
        }
      </div>
      {name}
    </div>
  )
};

// No Image
export const NoImage = () => <div className={css.noImage}>NO IMAGE</div>
