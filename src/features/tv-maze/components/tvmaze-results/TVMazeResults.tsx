import React from 'react';
import { Series } from '../../model/series';
import css from '../../PageDemoTVMaze.module.css';
import { TVMazeThumb } from './TVMazeThumb';
import { TotalResults } from './TotalResults';

export interface TVMazeResultsProps {
  result: Series[];
  open: (url: string) => void;
}

export const TVMazeResults: React.FC<TVMazeResultsProps> = props => {
  const { open, result } = props;

  return (
    <>
      <TotalResults value={result.length}/>

      <div  className={css.grid}>
        {
          result.map(series => {
            const { show: { id, name, image, url } } = series;
            return (
              <div key={id} className={css.gridItem}>
                <TVMazeThumb
                  name={name}
                  image={image}
                  thumbClick={() => open(url)}
                />
              </div>
            )
          })
        }
      </div>
    </>
  );
}

