import React from 'react';


// TotalResults.tsx
export const TotalResults: React.FC<{value: number}> = ({value}) => (
  value ? <h1>{value} series</h1> : <h2>There are no results</h2>
);
