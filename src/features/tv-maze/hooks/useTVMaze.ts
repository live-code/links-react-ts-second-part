import { useState } from 'react';
import { Series } from '../model/series';
import Axios from 'axios';

export function useTVMaze() {
  const [ result, setResult ] = useState<Series[]>([]);

  function openLinkHandler(url: string) {
    window.open(url)
  }

  const search = (text: string) => {
    Axios.get<Series[]>(`http://api.tvmaze.com/search/shows?q=${text}`)
      .then(res => {
        setResult(res.data);
      });
  };

  return {
    result,
    openLink: openLinkHandler,
    search
  }
}
