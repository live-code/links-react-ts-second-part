export interface Product {
  qty:  number;
  available: boolean;
}
