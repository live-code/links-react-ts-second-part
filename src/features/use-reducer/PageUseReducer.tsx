import React, { useReducer } from 'react';
import { Product } from './product';

const initialValue: Product = {
  qty: 0,
  available: false,
};

interface Action {
  type: string;
  payload?: number;
}

function reducer(state: Product, action: Action) {
  switch (action.type) {
    case 'increment':
      return { qty: state.qty + 1, available: true }
    case 'incByValue':
        return action.payload ?
          { qty: state.qty + action.payload, available: true } : state;
    case 'decrement':
      if (state.qty > 0) {
        const qty = state.qty - 1;
        const available = !(qty === 0)
        return { qty, available }
      }
      return state;
    case 'reset':
      return {...initialValue}
  }
  return state;
}


export const PageUseReducer: React.FC = () => {
  const [state, dispatch] = useReducer(reducer, initialValue)

  return <div>
    <div>Qty: {state.qty} </div>
    <div>Available: {state.available.toString()}</div>

    <button onClick={() => dispatch({ type: 'incByValue', payload: 10})}>+ 10</button>
    <button onClick={() => dispatch({ type: 'increment'})}>+</button>
    <button onClick={() => dispatch({ type: 'decrement'})}>-</button>
    <button onClick={() => dispatch({ type: 'reset'})}>reset</button>
  </div>
}


